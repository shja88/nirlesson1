#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <errno.h>
#include <signal.h>

int main()
{
	pid_t cpid=fork();
	if(cpid==0)
	{
		while(1)
		{	
			printf("I'm still alive\n");
		}	
	}
	else
	{
		sleep(1);
		printf("Dispatching\n");
		kill(cpid,SIGKILL);	
		printf("Dispatched\n");
	}
	return 0;
}
