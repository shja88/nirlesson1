#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <errno.h>
#include <signal.h>

void quit()
{
	char sen[1024];
	printf("are you want to exit\n");
	scanf("%s",sen);
	if(sen[0]=='y' || sen[0]=='Y')
	{
		syscall(SYS_exit,0);
	}
}

int main()
{
	int i;
	signal(2, quit);
	signal(3, quit);
	signal(15, quit);
	for(i=0;i<2000000000;i++)
	{
		if(i%1000000==0)
		{
			printf("I'm still alive i=%d\n",i);
		}
	}	
	exit(EXIT_SUCCESS);
	return 0;
}
