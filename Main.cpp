#include <iostream>
#include <thread>
#include <mutex>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

mutex mtx;
fstream file("myfile.txt");

void read()
{
	mtx.lock();
	string str;
	if (getline(file, str)){
		cout << "next time write to file" << endl;
		this_thread::sleep_for(std::chrono::seconds(5));
	}
	cout << str << endl;
	while (getline(file, str))
		cout << str << endl;
	mtx.unlock();
}

void write()
{
	mtx.lock();
	string str;
	cout << "enter line to file" << endl;
	cin >> str;
	file.write((char *)&str,sizeof(string));

	mtx.unlock();
}

int main()
{
	vector<thread> trds;
	int choice = 3;
	thread t;
	while (choice != 0)
	{
		cout << "what are you want to do with the file" << endl;
		cout << "0.exit" << endl;
		cout << "1.write" << endl;
		cout << "2.read" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			t = thread(read);
			trds.push_back(t);
			break;
		case 2:
			if (file.is_open())
			{
				t = thread(read);
				trds.push_back(t);
			}
			break;
		default:
			if (choice!=0)
			cout << "wrong choice" << endl;
			break;
		}
		for (unsigned int i = 0; i < trds.size(); i++)
			trds[i].join();
	}
	system("PAUSE");
	file.close();
	return 0;
}