#include "ceaser.h"


char shift_letter(char letter, int offset)
{
	int temp,check;
	check=offset/26;
	offset-=check*26;
	if (letter+offset>122)
	{
		temp = letter+offset-123;
		return 97 + temp;
	}
	if (letter+offset<97)
	{
		temp = letter+offset-96;
		return 122 + temp;
	}			
	return letter+offset;	
}

char * shift_string(char * input, int offset)
{
	int length = strlen(input);
	int i;
	char *encrypted=(char*)malloc(sizeof(char)*length);
	
	for (i = 0; i < length; ++i)
		encrypted[i] = shift_letter(input[i],offset);
	//strcpy(input,encrypted);
	free(encrypted);
	return input;
}
