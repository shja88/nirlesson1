#include <windows.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <stdio.h>

extern "C"
{
	__declspec(dllexport) void __cdecl output_message(const char* msg);
}


void __cdecl output_message(const char* msg)
{
	printf("%s\n",msg);
}

bool DLLMain(const char* msg)
{
	output_message(msg);
	return true;
}
