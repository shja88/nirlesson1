#include <windows.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <stdio.h>

#pragma warning(disable:4996)

int main(int argc,char* argv[])
{
	HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, false,atoi(argv[1]));
	if (proc == NULL)
	{
		printf("Failed to Open Process\n");
		printf("Error Code : %u\n", GetLastError());
		system("PAUSE");
		return 0;
	}
	if (0 == TerminateProcess(proc, 1000))
	{
		printf("Failed to terminated Process\n");
		printf("Error Code : %u\n", GetLastError());
		system("PAUSE");
		return 0;
	}
	printf("process close\n");
	CloseHandle(proc);
	system("PAUSE");
	return 0;
}