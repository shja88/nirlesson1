#include <windows.h>
#include <tlhelp32.h>
#include <tchar.h>
#include <stdio.h>
#include <string.h>

int part1()
{
	TCHAR szPath[MAX_PATH];
	if (!GetModuleFileName(0, szPath, MAX_PATH))
	{
		printf("Cannot get the path (%d)\n", GetLastError());
		return 0;
	}
	printf("the path: %S\n", szPath);
	if (0 == DeleteFile(szPath))
	{
		printf("Cannot delete the file (%d)\n", GetLastError());
		system("PAUSE");
		return 0;
	}
	return 1;
}

void part3()
{
	HANDLE batf;
	DWORD bytes;
	batf = CreateFile(L"delete.bat", GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	WriteFile(batf, "if del lesson11.2.exe call :de", strlen("if del lesson11.2.exe call :de"), &bytes, NULL);
	WriteFile(batf, ":de", strlen(":de"), &bytes, NULL);
	WriteFile(batf, "del lesson11.2.exe", strlen("del lesson11.2.exe"), &bytes, NULL);
	WriteFile(batf, "del delete.bat", strlen("del delete.bat"), &bytes, NULL);
	CloseHandle(batf);
	system("delete.bat");
}

int main()
{
	HANDLE batf, txtf;
	batf = CreateFile(L"deleter.bat",GENERIC_READ,0,NULL,CREATE_NEW,FILE_ATTRIBUTE_NORMAL,NULL);
	txtf = CreateFile(L"a.txt", GENERIC_READ, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD bytes;
	WriteFile(batf, "del a.txt\ndel deleter2.bat", strlen("del a.txt\ndel deleter2.bat"), &bytes, NULL);
	system("deleter.bat");
	part3();
	CloseHandle(batf);
	CloseHandle(txtf);
	return part1();
}