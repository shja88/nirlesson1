#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

typedef void(_cdecl *myfunc)(char *msg);

int main(int argc, char* argv[])
{
	HMODULE lh = LoadLibraryA("gui_plugin.dll");
	if (!LoadLibraryA("gui_plugin.dll"))
	{
		printf("don't work1\n");
		printf("Error Code : %u\n", GetLastError());
		system("PAUSE");
		return 0;

	}
	myfunc func = (myfunc)GetProcAddress(lh, "output_message");
	if (!GetProcAddress(lh, "output_message"))
	{
		printf("don't work2\n");
		printf("Error Code : %u\n", GetLastError());
		system("PAUSE");
		return 0;
	}
	func(argv[1]);
	FreeLibrary(lh);
	system("PAUSE");
	return 0;
}