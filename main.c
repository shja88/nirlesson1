#include "ceaser.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	if (argc < 4)
	{
		printf("usage: %s <mode> <message> <key>\n",argv[0]);
		exit(1);
	}
	int len=strlen(argv[2]),i;
	int mode = (argv[1][0] == 'd') ? -1 : 1;
	int key = atoi(argv[3]);
	char *input = (char*)malloc(sizeof(char)*len);
	for(i=0;i<len;i++)
	{
		if(argv[2][i]<97 || argv[2][i]>122)
		{
			printf("the message should be written in english\n");
			exit(1);
		}
	}
	input = shift_string(argv[2],mode * key * -1);
	printf("%s\n",input);
	free(input);
	return 0;
}
