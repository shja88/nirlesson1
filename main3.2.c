#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "helper.2.h"

#define STDOUT 1

void funexit()
{
	syscall(SYS_exit,0);	
}
void myid()
{	
	char *str1="my id is:";
	int myid = getuid();
	syscall(SYS_write,STDOUT, str1, slen(str1));
	syscall(SYS_write,STDOUT, __itoa(myid),slen(__itoa(myid)));
}
void uptime()
{
	char *str2="\nprocees that run together:",*str1="uptime:";
 	struct sysinfo myinfo;
 	sysinfo (&myinfo);
	syscall(SYS_write,STDOUT,str1,slen(str1));
	syscall(SYS_write,STDOUT,__itoa(myinfo.uptime),slen(__itoa(myinfo.uptime)));
	syscall(SYS_write,STDOUT,str2,slen(str2));
	syscall(SYS_write,STDOUT,__itoa(myinfo.procs),slen(__itoa(myinfo.procs)));
}

int main()
{
	int i;
	char *strmenu="\n1.exit\n2.id\n3.uptime\n\n",*num=(char*)malloc(sizeof(char));
	fun_desc funmenu[3];
	funmenu[0].name=funexit;
	funmenu[1].name=myid;
	funmenu[2].name=uptime;
	funmenu[0].fun=funexit;
	funmenu[1].fun=myid;
	funmenu[2].fun=uptime;
	while(1)
	{
		syscall(SYS_write,STDOUT,strmenu,slen(strmenu));
		syscall(SYS_read,STDOUT,num,2);
		i=atoi(num);
		funmenu[i-1].fun();
	}
	free(num);
	return 0;
}
