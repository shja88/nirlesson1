#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "helper.h"

#define STDOUT 1
#define STDIN 0

void exitf()
{
	syscall(SYS_exit,0);	
}
void printid()
{	
	char *str1="id:";
	int id = getuid();
	syscall(SYS_write,STDOUT, str1, slen(str1));
	syscall(SYS_write,STDOUT, __itoa(id),slen(__itoa(id)));
}
void printuptime()
{
	char *str1="\n",*str2="uptime:",*str3="procees run together:";
 	struct sysinfo info;
 	sysinfo (&info);
	syscall(SYS_write,STDOUT,str2,slen(str2));
	syscall(SYS_write,STDOUT,__itoa(info.uptime),slen(__itoa(info.uptime)));
	syscall(SYS_write,STDOUT,str1,slen(str1));
	syscall(SYS_write,STDOUT,str3,slen(str3));
	syscall(SYS_write,STDOUT,__itoa(info.procs),slen(__itoa(info.procs)));
}

int main()
{
	char *cnum=(char*)malloc(sizeof(char));
	int i;
	fun_desc menu[3];
	menu[0].name=exitf;
	menu[1].name=printid;
	menu[2].name=printuptime;
	menu[0].fun=exitf;
	menu[1].fun=printid;
	menu[2].fun=printuptime;
	char *str1="\n1.exit\n2.id\n3.uptime\n";
	while(1)
	{
		syscall(SYS_write,STDOUT,str1,slen(str1));
		syscall(SYS_read,STDOUT,cnum,2);
		i=atoi(cnum);
		menu[i-1].fun();
	}
	free(cnum);
	return 0;
}
