#include "mm.h"
#include <stdio.h>
#include <string.h>

typedef struct metadata_block * p_block; 
struct metadata_block 
{
	size_t size;  
	p_block  next;	
	int free; 
}; 

void *malloc(int size)
{
	sbrk(size);
	void *ptr[size];
	return ptr;
}

void *realloc(void *ptr,int size)
{
	sbrk(size);
	void *ptr2[size];
	ptr2[0]=ptr;	
	return ptr2;
}

void free(void *ptr)
{
	p_block bl=ptr;
	p_block bl2=bl;
	int len=0,i;
	while(bl->next)
	{
		len++;
		bl=bl->next;
	}
	bl=bl2;
	p_block bl3[len];	
	for(i=0;i<len;i++)
	{
		bl3[i]=bl;
		bl=bl->next;		
	}
	bl=bl2;
	for(i=len-2;i>=0;i--)
	{
		bl3[i]->next='NULL';
	}
}
