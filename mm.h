#ifndef shell_mm_h
#define shell_mm_h

void *malloc(int size);
void *realoc(void *ptr,int size);
void free(void *ptr);

#endif
