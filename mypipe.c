#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <errno.h>

int main()
{
	pid_t cpid;
	char str1[]="magshimim",str2[20];
	int arrpipe[2],n,run;
	pipe(arrpipe);
	if(cpid=fork()==0)
	{
		close(arrpipe[0]);
		write(arrpipe[1],str1,strlen(str1)+1);
		exit(0);
	}
	else
	{
		wait(&run);
		close(arrpipe[1]);
		read(arrpipe[0],str2,sizeof(str2));
		printf("the message: %s\n",str2);
	}	
	return 0;
}
