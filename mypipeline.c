#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <errno.h>

int main()
{
	int mypipe[2],fd=open(stdout),run;
	pipe(mypipe);
	pid_t cpid1,cpid2;
	if(cpid1=fork()==0)
	{	
		
		fd=dup(mypipe[1]);
		close(fd);
		execvp("ls -l");
		close(mypipe[1]);
	}
	if(cpid2=fork==0)
	{
		
		fd=dup(mypipe[0]);
		close(fd);
		execvp("tail -n 2");
		close(mypipe[0]);
	}
	else
	{
		wait(&run);
	}
	return 0;
}
