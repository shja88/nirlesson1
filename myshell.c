#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <linux/limits.h>
#include "LineParser.h"
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <errno.h>
	
void execute(cmdLine *pCmdLine)
{
	pid_t cpid = fork();
	if(cpid==0)
	{
		execvp(pCmdLine->arguments[0],pCmdLine->arguments);
		perror("ERROR");
	}
	else
	{
		waitpid(-1,cpid,0);
	}
/*
the diffrent bettwen execv and execvp is that: 
in execv you insert th path and in execvp you insert file  
*/
}

int main()
{	
	int i;
	cmdLine *pCmdLine;
	char path[PATH_MAX],linein[2048],quit[]="quit",canquit[5],str1[100];
	while(1)
	{	
		getcwd(path,PATH_MAX);
		printf("%s$ ",path);
		fgets(linein,2048,stdin);
		for(i=0;i<4;i++)
			canquit[i]=linein[i];
		if(!strcmp(quit,canquit))
		{
			freeCmdLines(pCmdLine);
			return 0;
		}	
		pCmdLine=parseCmdLines(linein);	
		if(!strcmp(pCmdLine->arguments[0],"myecho"))
		{
			for(i=0;i<100;i++)
				str1[i]=linein[i+7];
			printf("%s",str1);	
		}
		else
		{
			if(!strcmp(pCmdLine->arguments[0],"cd"))	
			{
				for(i=0;i<100;i++)
					str1[i]=linein[i+3];
				i=chdir(str1);
				if(i!=0)
					printf("chdir faild\n");
			}
			else
			{
				execute(pCmdLine);
			}
		}			
	}
	freeCmdLines(pCmdLine);
	return 0;
}
