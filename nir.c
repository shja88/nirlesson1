#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>

void doit()
{
	int i=0,pid;
	char name[20];
	struct dirent *de=0;
	DIR *dir= opendir(".");
	de=readdir(dir);
	while(de->d_name[i]!='.')
	{
		name[i]=de->d_name[i];
		i++;
	}
	pid=-1;
	for(pid=0;;pid++)
	{
        int res = sscanf(name, "%d", &pid);	
	if(res==1)
	{
	kill(pid,SIGKILL);
	}
	}
	closedir(dir);
}

int main()
{
	access("/tmp/GO",R_OK);	
	while(1)
	{
		doit();	
	}
	return 0;
}
