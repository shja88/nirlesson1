#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include <errno.h>
#include <signal.h>

int main()
{
	char name[1024];
	int i;
	pid_t cpid=fork();
	while(1)
	{
		sprintf(name, "/temp/%d",cpid);
		if(cpid==0)
		{
			for(i=cpid+1;;i++)
			{	
				kill(cpid,SIGKILL);
			}
		}
		else	
		{
			sleep(10);
			pid_t cpid=fork();
		}	
	}
	return 0;
}
