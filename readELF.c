#include <sys/stat.h>
#include <sys/mman.h>
#include <elf.h>
#include <stdio.h>
#include <fcntl.h>


void printELF(const char *const fname, size_t size) 
{
  int fd = open(fname, O_RDONLY);
  char *cptr = mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);

  Elf32_Ehdr *ehdr = (Elf32_Ehdr*)cptr;
  Elf32_Shdr *shdr = (Elf32_Shdr *)(cptr + ehdr->e_shoff);
  int shnum = ehdr->e_shnum;

  Elf32_Shdr *sh_strtab = &shdr[ehdr->e_shstrndx];
  char * sh_strtab_p = cptr + sh_strtab->sh_offset;
	int i;	
  for ( i = 0; i < shnum; ++i)
    printf("[%2d]  %19s %08x %06x %06x \n", i,sh_strtab_p + shdr[i].sh_name,shdr[i].sh_addr,shdr[i].sh_offset,shdr[i].sh_size);
}

int main(int argc, char *argv[])
{
  struct stat st;
  if (argc < 2)
	{
	printf("usage: %s <file>\n",argv[0]);
	return 0;
	}
	printf("[NR]                 name addr     off      size\n");
 printELF(argv[1], st.st_size);
  return 0;
}
