#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

int main()
{
	int linenum=0,i;
	char note;
	srand(time(NULL));
	FILE *f=fopen("task2.txt","r");
	if(!f)
	{
		printf("we couldn't open this file/n");
		exit(1);
	}
	fseek(f,0,SEEK_SET);
        while ((note =fgetc(f))!=EOF) 
        {
        	if (note=='\n')
		{	
			linenum++;		
		}
        }
	fseek(f,0,SEEK_SET);
	int ranum = rand() % linenum ;
	for(i=0;i<ranum;)
	{
		note =fgetc(f);
		if (note=='\n') 
        	{
			i++;
		}	
	}		
	printf("line %d: ",ranum+1);
	while((note=fgetc(f))!='\n')
	{
		printf("%c",note);
	}	
	printf("\n");
	fclose(f);
	return 0;
}
